This project was moved to https://gitlab.com/frederic-zinelli/tkutils.

Related documentation on: https://frederic-zinelli.gitlab.io/tkutils/

Old links are permanently redirected where appropriate (301).
